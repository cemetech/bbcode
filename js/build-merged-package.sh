#!/usr/bin/env bash
#
# This script builds the javascript library for both Node and browser
# using wasm-pack, then merges those into a single package that can be used
# for both (provided the browser is using a bundler).
#
# wasm-pack doesn't currently support something like this
# (see https://github.com/rustwasm/wasm-pack/issues/313), but if we assume
# the generated wasm is the same for any environment then we can merge the
# package manifests, copy the wasm wrappers, and call it good.
#
# This implementation is inspired by @zrzka's comment on the above-linked
# issue, but doesn't mind hard-coding as much.

# Build both targets
wasm-pack build --target bundler --scope cemetech --out-dir pkg
wasm-pack build --target nodejs --scope cemetech --out-dir pkg-node

# Copy the node sources into the "real" package
cp pkg-node/bbcode.js pkg/index.js
cp pkg-node/bbcode.d.ts pkg/index.d.ts
# Modify the package manifest to add the new files and mark the two different
# entrypoints.
jq '.files += ["index.js", "index.d.ts"]
   | .main = "index.js"
   | .browser = "bbcode.js"
' pkg/package.json > package.json
mv package.json pkg/package.json

# Build the final package
wasm-pack pack
