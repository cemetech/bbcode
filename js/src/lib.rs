use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn render(bbcode: &str) -> String {
    cemetech_bbcode::render(bbcode)
}
