use ::cemetech_bbcode as bbcode;
use pyo3::prelude::*;

/// Cemetech-flavor BBCode parser and renderer.
#[pymodule]
fn cemetech_bbcode(_py: Python, m: &Bound<'_, PyModule>) -> PyResult<()> {
    /// Convert the given BBCode markup to an HTML string.
    #[pyfunction]
    #[pyo3(text_signature = "(markup)")]
    fn render_str(_py: Python, markup: &str) -> String {
        bbcode::render(markup)
    }

    m.add_function(wrap_pyfunction!(render_str, m)?)?;
    Ok(())
}
