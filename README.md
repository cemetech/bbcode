# Cemetech-flavor BBCode library

This is a library for working with
[Cemetech](https://www.cemetech.net/)-flavor BBCode. The implementation is in
Rust, with bindings currently supported for Python and WASM bindings planned.

## Rust

The Rust library builds with cargo, as is typical for Rust crates:

    cargo build

## Python

Building the Python bindings requires a Rust compiler and the usual `setuptools`
pieces. Provided your dependencies are met, install with pip from a local
checkout:

    pip install -e .

Or without manually checking out, install from git:

    pip install git+https://gitlab.com/cemetech/bbcode#egg=cemetech_bbcode

Because building the extension module may be slightly difficult as it requires
additional tools (and seems picky in unclear ways, especially on Windows), CI
systems are set up to compile binary wheels that can be used on most Windows and
Linux systems. Given the file corresponding to the intended Python interpreter
and OS, install with pip. For example, for 32-bit Python 3.8 on Windows:

    pip install cemetech_bbcode-0.1.dev7+g8c3c2d7-cp38-cp38-win32.whl

The binaries are also published to the package index on Gitlab:
https://gitlab.com/cemetech/bbcode/-/packages. Further information on how to
install packages from that index is available there.