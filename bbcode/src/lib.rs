use bbcode::{self, DecorationStyle, ListStyle, SegmentHandler, Span};
use std::convert::Infallible;
use std::io::{Cursor, Write};
use std::iter::FromIterator;
use url::Url;

enum TextOnlyTag {
    Image,
    Youtube,
}

struct StringRenderer {
    buf: Cursor<Vec<u8>>,
    // Stack of link text being built up, for links that don't have targets
    // specified ahead of time ([url] vs [url=foo]). Because links may have
    // other blocks inside (including other links), each open link with the
    // target specified in the text gets text buffered here and emitted on
    // close.
    link_targets: Vec<String>,
    // True if current text is the body of an [img] tag, specifying the
    // URL of the image. [img] doesn't accept other tags in the body, so
    // it's okay to have a simple flag. No buffering is required because
    // there will only ever be one text span inside.
    in_text_only: Option<TextOnlyTag>,
}

/// Minimum size allowed for text in order to display it as text.
/// If smaller, contents are displayed as a spoiler instead.
const SIZE_PX_SPOILER_THRESHOLD: u8 = 4;
/// Minimum size allowed for text that is displayed as text. Smaller
/// text (that is not a spoiler) is clamped to this size.
const SIZE_PX_MIN_TEXT: u8 = 6;

impl<'a> SegmentHandler<'a> for &mut StringRenderer {
    type Err = Infallible;

    fn begin_span(&mut self, span: &Span) -> Result<(), Self::Err> {
        use DecorationStyle::*;
        use ListStyle::*;
        use Span::*;

        let _ = match span {
            Decorated(Bold) => write!(self.buf, r#"<span style="font-weight: bold">"#),
            Decorated(Underline) => {
                write!(self.buf, r#"<span style="text-decoration: underline">"#)
            }
            Decorated(Italic) => write!(self.buf, r#"<span style="font-style: italic">"#),
            Decorated(Center) => write!(self.buf, r#"<div style="text-align: center">"#),
            Decorated(Color(r, g, b)) => write!(
                self.buf,
                r#"<span style="color: #{:02x}{:02x}{:02x}">"#,
                r, g, b
            ),
            Decorated(Size(px)) => {
                let px = px.get();

                if px >= SIZE_PX_SPOILER_THRESHOLD {
                    // Assume regular font size is 12 and scale proportionally without using
                    // absolute measures. Note that because this uses an absolute measure,
                    // protected against text becoming too small via tag nesting (eg,
                    // [size=6][size=6] still makes 50%-size text rather than 25%).
                    let scale = std::cmp::max(SIZE_PX_MIN_TEXT, px) as f32 / 12.0;
                    write!(self.buf, r#"<span style="font-size: {:.3}rem">"#, scale)
                } else {
                    // Very small text is treated as a spoiler instead.
                    write!(self.buf, r#"<span class="bbcode-spoiler"><span>"#)
                }
            }
            Decorated(Strikethrough) => write!(self.buf, "<s>"),
            Decorated(Monospaced) => write!(self.buf, "<code>"),
            Decorated(Subscript) => write!(self.buf, "<sub>"),
            Decorated(Superscript) => write!(self.buf, "<sup>"),
            Quote(Some(attribution)) => write!(
                self.buf,
                "<div class=\"quote_name\">\
                   <span class=\"genmed\"><b>{} wrote:</b></span>\
                 </div>\
                 <div class=\"quote\">",
                attribution
            ),
            Quote(None) => write!(
                self.buf,
                "<div class=\"quote_name\">\
                   <span class=\"genmed\"><b>Quote:</b></span>\
                 </div>\
                 <div class=\"quote\">"
            ),
            Code => write!(
                self.buf,
                "<br>\
                 <span class=\"genmed\"><b>Code:</b></span>\
                 <br>\
                 <div class=\"code\">\
                 <code class=\"prettyprint\" style=\"font-family: Courier, 'Courier New', monospace\">"
            ),
            List(Unordered) => write!(self.buf, "<ul>"),
            List(Numeric) => write!(self.buf, r#"<ol style="list-style: decimal">"#),
            List(Alphabetic) => write!(self.buf, r#"<ol style="list-style: lower-latin">"#),
            ListItem(_) => write!(self.buf, "<li>"),
            Link(None) => {
                self.link_targets.push(String::new());
                Ok(())
            }
            Link(Some(unsafe_url)) => match safe_url(unsafe_url) {
                Some(url) => write!(self.buf, r#"<a href="{}" rel="ugc">"#, url),
                None => {
                    let _ = write!(self.buf, "[url=");
                    let _ = reverse_format_quoted(&mut self.buf, unsafe_url);
                    write!(self.buf, "]")
                }
            },
            // Target always in body text; do nothing
            Image => {
                debug_assert!(self.in_text_only.is_none());
                self.in_text_only = Some(TextOnlyTag::Image);
                Ok(())
            },
            YoutubeEmbed => {
                debug_assert!(self.in_text_only.is_none());
                self.in_text_only = Some(TextOnlyTag::Youtube);
                Ok(())
            }
            HorizontalRule => write!(self.buf, "<hr>"),
        };

        Ok(())
    }

    fn text(&mut self, s: &str) -> Result<(), Self::Err> {
        if let Some(tag) = &self.in_text_only {
            let _ = match tag {
                TextOnlyTag::Image => match safe_url(s) {
                    Some(url) => write!(self.buf, r#"<img src="{}" alt="">"#, url),
                    None => {
                        let _ = write!(self.buf, "[img]");
                        let _ = write_text_safe(&mut self.buf, s);
                        write!(self.buf, "[/img]")
                    }
                },
                TextOnlyTag::Youtube => match parse_youtube_url(s) {
                    Some((video_id, start_time)) => {
                        // Percent-encode the extracted video info before writing it out
                        let video_id = String::from_iter(url::form_urlencoded::byte_serialize(
                            video_id.as_bytes(),
                        ));
                        // Emit a whole query string for time
                        let start_time = start_time
                            .map(|raw_start_time| {
                                const PREFIX: &str = "?start=";
                                let mut qs =
                                    String::with_capacity(raw_start_time.len() + PREFIX.len());
                                qs += PREFIX;
                                qs.extend(url::form_urlencoded::byte_serialize(
                                    raw_start_time.as_bytes(),
                                ));
                                qs
                            })
                            .unwrap_or(String::new());
                        write!(
                            self.buf,
                            "<iframe width=\"560\" height=\"315\" \
                                                src=\"https://www.youtube.com/embed/{}{}\" \
                                                frameborder=\"0\" \
                                                allow=\"accelerometer;autoplay;clipboard-write;\
                                                        encrypted-media;gyroscope;\
                                                        picture-in-picture\" \
                                                allowfullscreen></iframe>",
                            video_id, start_time
                        )
                    }
                    None => {
                        let _ = write!(self.buf, "[youtube]");
                        let _ = write_text_safe(&mut self.buf, s);
                        write!(self.buf, "[/youtube]")
                    }
                },
            };
        } else if let Some(url) = self.link_targets.last_mut() {
            // Buffer link target URL
            url.push_str(s);
        } else {
            let _ = write_text_safe(&mut self.buf, s);
        }

        Ok(())
    }

    fn end_span(&mut self, span: &Span) -> Result<(), Self::Err> {
        use DecorationStyle::*;
        use ListStyle::*;
        use Span::*;

        let _ = match span {
            Decorated(Size(px)) if px.get() < SIZE_PX_SPOILER_THRESHOLD => {
                write!(self.buf, "</span></span>")
            }
            Decorated(Bold) | Decorated(Underline) | Decorated(Italic) | Decorated(Color(..))
            | Decorated(Size(..)) => write!(self.buf, "</span>"),
            Decorated(Center) => write!(self.buf, "</div>"),
            Decorated(Strikethrough) => write!(self.buf, "</s>"),
            Decorated(Monospaced) => write!(self.buf, "</code>"),
            Decorated(Subscript) => write!(self.buf, "</sub>"),
            Decorated(Superscript) => write!(self.buf, "</sup>"),
            Quote(_) => write!(self.buf, "</div>"),
            Code => write!(self.buf, "</code></div>"),
            List(Unordered) => write!(self.buf, "</ul>"),
            List(Numeric) | List(Alphabetic) => write!(self.buf, "</ol>"),
            ListItem(_) => write!(self.buf, "</li>"),
            Link(None) => {
                // Didn't know what the text was until we got the whole tag. If the URL
                // is legal then make a link, otherwise print it back out as markup.
                let unsafe_url = self.link_targets.pop().unwrap();
                match safe_url(&unsafe_url) {
                    Some(url) => {
                        let _ = write!(self.buf, r#"<a href="{}" rel="ugc">"#, url);
                        let _ = write_text_safe(&mut self.buf, unsafe_url);
                        write!(self.buf, "</a>")
                    }
                    None => {
                        let _ = write!(self.buf, "[url]");
                        let _ = write_text_safe(&mut self.buf, unsafe_url);
                        write!(self.buf, "[/url]")
                    }
                }
            }
            Link(Some(unsafe_url)) => {
                if safe_url(unsafe_url).is_some() {
                    write!(self.buf, "</a>")
                } else {
                    // Illegal URL was printed as markup
                    write!(self.buf, "[/url]")
                }
            }
            // Output is handled entirely on the text node
            Image | YoutubeEmbed => {
                self.in_text_only = None;
                Ok(())
            }
            // Self-closing, all emitted at the start
            HorizontalRule => Ok(()),
        };

        Ok(())
    }
}

pub fn render(markup: &str) -> String {
    let mut renderer = StringRenderer {
        // Output will always be at least as large as input
        buf: Cursor::new(Vec::with_capacity(markup.len())),
        link_targets: Vec::new(),
        in_text_only: None,
    };

    let _ = bbcode::walk_segments(markup, &mut renderer);

    String::from_utf8(renderer.buf.into_inner())
        .unwrap_or_else(|e| format!("Rendered output contained invalid UTF-8: {:?}", e))
}

const SAFE_URL_SCHEMES: &[&'static str] = &["http", "https"];

/// Safely output a URL, suitable for use in a quoted (") attribute.
///
/// Schemes must be part of SAFE_URL_SCHEMES (specifically to exclude
/// things like "javascript:alert('pwnd')". Other parts are URL-encoded.
/// If the input is not a valid URL then returns an empty string.
fn safe_url(s: &str) -> Option<String> {
    match Url::parse(s) {
        Ok(u) if SAFE_URL_SCHEMES.contains(&u.scheme()) => Some(u.into_string()),
        _ => None,
    }
}

/// Display the given URL as plain text, quoted if necessary.
///
/// This is used to "undo" parsing of URLs that turn out to be unacceptable,
/// either because they are unsafe or malformed. The provided string is written,
/// surrounded by escaped quotes if it contains none, otherwise unquoted.
/// The string is written with safe escaping in any case.
fn reverse_format_quoted<W: Write, S: AsRef<str>>(mut out: W, s: S) -> std::io::Result<()> {
    let s = s.as_ref();

    let delim = if !s.contains('"') {
        "&quot;"
    } else if !s.contains('\'') {
        "&#39;"
    } else {
        ""
    };
    write!(out, "{}", delim)?;
    write_text_safe(&mut out, s)?;
    write!(out, "{}", delim)
}

fn write_text_safe<W: Write, S: AsRef<str>>(mut out: W, s: S) -> std::io::Result<()> {
    const SPECIAL_CHARS: &[char] = &['\n', '&', '<', '>', '"', '\''];

    let mut s = s.as_ref();
    // TODO auto-linking, translation of emotes..
    while let Some(idx) = s.find(SPECIAL_CHARS) {
        let replacement = match s[idx..].chars().next().unwrap() {
            '\n' => "<br>\n",
            '&' => "&amp;",
            '<' => "&lt;",
            '>' => "&gt;",
            '"' => "&quot;",
            // OWASP recommend numeric escape because HTML 4 doesn't define &apos; (but XML
            // and XHTML do, as does HTML5).
            '\'' => "&#39;",
            _ => unreachable!(),
        };
        write!(out, "{}{}", &s[..idx], replacement)?;
        s = &s[idx + 1..];
    }
    // Tail is known not to have any special characters
    write!(out, "{}", s)
}

/// Parse the video ID and optional start time out of a youtube URL.
///
/// The results are not guaranteed to be URL-safe, so the caller must escape
/// them as required.
///
/// Accepts regular video links on youtube.com, such as https://www.youtube.com/watch?v=K-LzHn7G-h8
/// or short links like https://youtu.be/K-LzHn7G-h8. If a `t` query parameter is present it is
/// treated
fn parse_youtube_url(s: &str) -> Option<(String, Option<String>)> {
    let url = Url::parse(s).ok()?;
    let domain = url.domain()?;

    if domain == "youtu.be" {
        let video_id = url.path_segments()?.next()?;
        let start_time = url
            .query_pairs()
            .find(|(k, _)| k == "t")
            .map(|(_, v)| String::from(v.as_ref()));

        Some((video_id.into(), start_time))
    } else if ["youtube.com", "www.youtube.com"].contains(&domain) {
        if url.path() != "/watch" {
            return None;
        }
        let mut video_id: Option<String> = None;
        let mut start_time: Option<String> = None;

        for (k, v) in url.query_pairs() {
            if k == "v" {
                video_id = Some(v.into_owned());
            } else if k == "t" {
                start_time = Some(v.into_owned());
            }
        }
        Some((video_id?, start_time))
    } else {
        None
    }
}

#[test]
fn safe_url_escapes_special_chars() {
    assert_eq!(
        safe_url("http://example.com/\"\'<>"),
        Some("http://example.com/%22'%3C%3E".into())
    );
}

#[test]
fn validates_src_href_safety() {
    // Unsafe URLs cause markup to be ignored
    assert_eq!(
        &render("[url]javascript:alert('')[/url]"),
        "[url]javascript:alert(&#39;&#39;)[/url]"
    );
    assert_eq!(
        &render("[url=javascript:alert(\"<lol>\")]Click me![/url]"),
        "[url=&#39;javascript:alert(&quot;&lt;lol&gt;&quot;)&#39;]Click me![/url]"
    );
    assert_eq!(
        &render("[img]javascript:alert('Cookie: ' + document.cookie)[/img]"),
        "[img]javascript:alert(&#39;Cookie: &#39; + document.cookie)[/img]"
    );

    // Allowed URLs are okay, but attributes remain escaped
    assert_eq!(
        &render("[url]https://example.com/\"[/url]"),
        "<a href=\"https://example.com/%22\" rel=\"ugc\">https://example.com/&quot;</a>"
    );
    assert_eq!(
        &render("[url=http://example.net/?quote='hello'&text=true]hello[/url]"),
        "<a href=\"http://example.net/?quote=%27hello%27&text=true\" rel=\"ugc\">hello</a>"
    );
    assert_eq!(
        &render("[img]http://example.com/image.php?x=5[/img]"),
        r#"<img src="http://example.com/image.php?x=5" alt="">"#
    );
}

#[test]
fn write_escaped_escapes() {
    let mut buf = Cursor::new(Vec::new());
    let _ = write_text_safe(&mut buf, "<script>Get \"ow&\", loser!</script>");
    assert_eq!(
        std::str::from_utf8(&buf.into_inner()).unwrap(),
        "&lt;script&gt;Get &quot;ow&amp;&quot;, loser!&lt;/script&gt;"
    );
}

/// Text smaller than the spoiler threshold is rendered as a spoiler.
#[test]
fn size_spoiler_transform() {
    assert_eq!(
        &render("[size=3]Too small to read. :^)[/size]"),
        r#"<span class="bbcode-spoiler"><span>Too small to read. :^)</span></span>"#
    );
}

/// Text smaller than the minimum regular text size is increased to the minimum.
#[test]
fn size_clamp() {
    assert_eq!(
        &render("[size=4]I'm small![/size]"),
        r#"<span style="font-size: 0.500rem">I&#39;m small!</span>"#
    );
}

#[test]
fn kitchen_sink() {
    let result = render(
        r#"
[b]Bold[/b]
[u]Underlined[/u]
[i]italicized[/i]
[center]Centered[/center]
[color=red]RED[/color]
[size=6]Half size[/size]
[strike]Overline[/strike]
[mono]Monospaced[/mono]
[sub]subscript[/sub]
[sup]superscript[/sup]
[quote="Me"]Quoting myself[/quote]
[quote]Quoting somebody[/quote]
[code]<body>[/code]
[list][*] Item[/list]
[list=1][*] One[*] Two[/list]
[list=a][*] a[/list]
[youtube]https://youtu.be/Jrg0X9H6FGU?t=28[/youtube]
[hr]"#,
    );

    assert_eq!(
        &result,
        r#"<br>
<span style="font-weight: bold">Bold</span><br>
<span style="text-decoration: underline">Underlined</span><br>
<span style="font-style: italic">italicized</span><br>
<div style="text-align: center">Centered</div><br>
<span style="color: #ff0000">RED</span><br>
<span style="font-size: 0.500rem">Half size</span><br>
<s>Overline</s><br>
<code>Monospaced</code><br>
<sub>subscript</sub><br>
<sup>superscript</sup><br>
<div class="quote_name"><span class="genmed"><b>Me wrote:</b></span></div><div class="quote">Quoting myself</div><br>
<div class="quote_name"><span class="genmed"><b>Quote:</b></span></div><div class="quote">Quoting somebody</div><br>
<br><span class="genmed"><b>Code:</b></span><br><div class="code"><code class="prettyprint" style="font-family: Courier, 'Courier New', monospace">&lt;body&gt;</code></div><br>
<ul><li> Item</li></ul><br>
<ol style="list-style: decimal"><li> One</li><li> Two</li></ol><br>
<ol style="list-style: lower-latin"><li> a</li></ol><br>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Jrg0X9H6FGU?start=28" frameborder="0" allow="accelerometer;autoplay;clipboard-write;encrypted-media;gyroscope;picture-in-picture" allowfullscreen></iframe><br>
<hr>"#
    );
}
