use cemetech_bbcode::render;

fn main() -> Result<(), std::io::Error> {
    let args = std::env::args_os().skip(1).collect::<Vec<_>>();
    if args.len() > 2 {
        eprintln!(
            "Usage: bbcode2html [infile] [outfile]

Infile and outfile default to standard input and standard output respectively,\
if unspecified."
        );
        std::process::exit(1);
    }

    let mut infile: Box<dyn std::io::Read> = match args.get(0) {
        Some(path) => Box::new(std::fs::File::open(path)?),
        None => Box::new(std::io::stdin()),
    };
    let mut outfile: Box<dyn std::io::Write> = match args.get(1) {
        Some(path) => Box::new(std::fs::File::create(path)?),
        None => Box::new(std::io::stdout()),
    };

    let mut markup = Vec::new();
    infile.read_to_end(&mut markup)?;

    let markup = String::from_utf8_lossy(&markup);
    outfile.write_all(render(&markup).as_bytes())?;

    Ok(())
}
